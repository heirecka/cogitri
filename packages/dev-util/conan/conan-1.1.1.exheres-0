# Copyright 2017-2018 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'conan-0.24.0.ebuild' from Gentoo, which is:
#     Copyright 1999-2017 Gentoo Foundation

require setup-py [ import=setuptools python_opts="[sqlite]" test=nose ]
require pypi

SUMMARY="Distributed C/C++ package manager"
HOMEPAGE="https://conan.io/"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~x86"

MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/astroid[~>1.6.0][python_abis:*(-)?]
        dev-python/bottle[>=0.12.8][python_abis:*(-)?]
        dev-python/colorama[~>0.3.3][python_abis:*(-)?]
        dev-python/distro[>=1.0.2][python_abis:*(-)?]
        dev-python/fasteners[>=0.14.1][python_abis:*(-)?]
        dev-python/future[=0.16.0][python_abis:*(-)?]
        dev-python/node-semver[=0.2.0][python_abis:*(-)?]
        dev-python/passlib[>=1.6.5][python_abis:*(-)?]
        dev-python/patch[=1.16][python_abis:*(-)?]
        dev-python/pluginbase[>=0.5][python_abis:*(-)?]
        dev-python/Pygments[~>2.0][python_abis:*(-)?]
        dev-python/PyJWT[~>1.4][python_abis:*(-)?]
        dev-python/pylint[~>1.8.1][python_abis:*(-)?]
        dev-python/PyYAML[>=3.11&<3.13][python_abis:*(-)?]
        dev-python/requests[~>2.7][python_abis:*(-)?]
        dev-python/six[>=1.10][python_abis:*(-)?]
    test:
        dev-python/coverage[python_abis:*(-)?]
        dev-python/mock[python_abis:*(-)?]
        dev-python/nose-parameterized[python_abis:*(-)?]
        dev-python/WebTest[~>2.0.18][python_abis:*(-)?]
"

# Doesn't play nice with sydbox, requires complete internet access
RESTRICT="test"

# The tests contain faulty code to test conan
PYTHON_BYTECOMPILE_EXCLUDES=( test )

prepare_one_multibuild() {
    setup-py_prepare_one_multibuild

    # Relax dependency on distro, 1.2.0 works fine
    edo sed -e "s:, <1.2.0::" -i conans/requirements.txt
}

test_one_multibuild() {
    esandbox allow_net --connect "unix:/run/uuidd/request"

    setup-py_test_one_multibuild

    esandbox disallow_net --connect "unix:/run/uuidd/request"
}

pkg_postinst() {
    ewarn "Please note that conan's dependencies have been relaxed,"
    ewarn "to make conan packageable for us. The upper limit of dev-python/distro's "
    ewarn "version has been removed. Please don't report bugs upstream before trying"
    ewarn "to reproduce the bug with the by upstream recommended version of distro."
}

