# Copyright 2012 NAKAMURA Yoshitaka
# Copyright 2013 Nicolas Braud-Santoni <nicolas+exherbo@braud-santoni.eu>
# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 1.15 ] ]
require github [ user=zfsonlinux project=zfs release=zfs-${PV} pn=${PN} suffix=tar.gz ]

export_exlib_phases src_prepare src_install pkg_postinst

SUMMARY="Solaris Porting Layer"
HOMEPAGE="http://zfsonlinux.org/"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES=""

DEFAULT_SRC_CONFIGURE_PARAMS=( --with-config=user )

# XXX: need root privileges. also, kernel modules should be build to run test
RESTRICT="test"

AT_M4DIR=( config )

spl_src_prepare() {
    # Use our prefixed cpp
    edo sed -i -e "s/cpp/$(exhost --target)-cpp/" config/spl-build.m4

    eautoreconf
}

spl_src_install() {
    default

    insinto /usr/src/${PNV}
    doins -r META config include module
    exeinto /usr/src/${PNV}
    doexe configure

    find ./ -name "*.in" | while read n; do
        insinto /usr/src/${PNV}/"$(dirname "${n}")"
        doins "${n}"
    done
}

spl_pkg_postinst() {
    elog "The kernel module source is installed into /usr/src/${PNV}"
    elog "You have to build it yourself now and for every consequent kernel update:"
    elog "# cd /usr/src/${PNV}"
    elog "# ./configure --with-config=kernel --with-linux=<pathtoyourkernelsource>"
    elog "# make && make install"
}

